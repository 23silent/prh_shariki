$('.main-slider').slick({
    speed: 1000,
    dots: true,
    arrows: false,
    fade: true,
    // autoplay: false,
    autoplay: true,
    autoplaySpeed: 4000
});

$('button[data-toggle="collapse"]').on('click', function() {
    $('body').toggleClass('fixed');
});
$('.dropdown-toggle').on('click', function() {
    $(this).toggleClass('open');
})

$('.recommend-carousel').slick({
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows: true,
    prevArrow: '<button type="button" class="slick-prev">еуыы</button>',
});

if ($(window).width() > 1024) {
    $(".img_zoom").elevateZoom();
}

$(window).resize(function() {

    if ($(window).width() > 1024) {
        $(".img_zoom").elevateZoom();
    }
});


/* Фон верхнего меню при скролле
====================================*/


// var headerH = $("#js-header").height(),
//     navH = $("#js-nav-container").innerHeight();

$(document).on("scroll", function() {

    var documentScroll = $(this).scrollTop();

    if (documentScroll > 1) {
        $("#js-nav-container").addClass("top-menu-bg");

        // $("#js-header").css({
        //     "paddingTop": navH
        // });
    } else {
        $("#js-nav-container").removeClass("top-menu-bg");
        // $("#js-header").removeAttr("style");
    }

});
